const _consumer_key = 'fSVCc53aq8mEtc3UpaE1FTQTR';
const _consumer_secret = 'IOQ8YuoYHbNLIRCQ9TQRFojgbXs5fuEy1ncbUh9c0CFf2IvQY6';
const _redirectURL = 'http://localhost:4200';

const express = require('express');
const session = require('express-session');
const OAuth = require('oauth').OAuth;
const Twitter = require('twit');

const app = express();
const client = new Twitter({
  consumer_key: _consumer_key,
  consumer_secret: _consumer_secret,
  access_token: 'token',
  access_token_secret: 'token_secret'
});


const oa = new OAuth(
  "https://twitter.com/oauth/request_token",
  "https://twitter.com/oauth/access_token",
  _consumer_key,
  _consumer_secret,
  "1.0A",
  _redirectURL,
  "HMAC-SHA1"
);


app.use(require('cors')());
app.use(require('body-parser').json());


app.use(session({secret: 'me secret', resave: false, saveUninitialized: true}));
app.use((req, res, next) => {
  res.locals.session = req.session;
  next();
});


app.get('/api/login', (req, res) => {
  oa.getOAuthRequestToken((error, oAuthToken, oAuthTokenSecret, results) => {
    if (error) {
      res.send(error, 500);
    } else {
      req.session.oauthRequestToken = oAuthToken;
      req.session.oauthRequestTokenSecret = oAuthTokenSecret;
      const redirect = {
        redirectUrl: `https://twitter.com/oauth/authorize?oauth_token=${oAuthToken}`
      };
      res.send(redirect);
    }
  })
});

app.get('/api/saveAccessTokens', (req, res) => {
  oa.getOAuthAccessToken(
    req.query.oauth_token,
    req.session.oauthRequestTokenSecret,
    req.query.oauth_verifier,
    (error, oauthAccessToken, oauthAccessTokenSecret, results) => {
      if (error) {
        return res.send({message: 'KO'})
      } else {
        req.session.oauthAccessToken = oauthAccessToken;
        req.session.oauthAccessTokenSecret = oauthAccessTokenSecret;
        _createClient(oauthAccessToken, oauthAccessTokenSecret);
        return res.send({message: 'OK'});
      }
    });
});

app.get('/api/user', (req, res) => {
  console.log(client)
  client.get('account/verify_credentials')
    .then(user => {
      res.send(user);
    })
    .catch(error => {
      res.send(error);
    });
});

app.get('/api/home', (req, res) => {
  const params = {tweet_mode: 'extended', count: req.query.count};
  client.get(`statuses/home_timeline`, params)
    .then(timeline => {
      res.send(timeline);
    })
    .catch(error => res.send(error));

});
app.get('/api/search', (req, res) => {

  const params = {q: req.query.q, count: req.query.count, lang: req.query.lang};

  client.get(`search/tweets`, params)
    .then(timeline => {
      res.send(timeline);
    })
    .catch(error => res.send(error));

});

function _createClient(oauthAccessToken, oauthAccessTokenSecret) {
  client.setAuth({
    consumer_key: client.getAuth().consumer_key,
    consumer_secret: client.getAuth().consumer_secret,
    access_token: oauthAccessToken,
    access_token_secret: oauthAccessTokenSecret
  });
}

app.listen(3000, () => console.info('Server running: 3000'));
